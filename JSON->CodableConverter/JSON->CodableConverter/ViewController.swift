//
//  ViewController.swift
//  JSON->CodableConverter
//
//  Created by Piotr Krzesaj on 14/01/2018.
//  Copyright © 2018 pkTeam. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        do{
            let fileMan = Bundle.main.path(forResource: "testJSON", ofType: "txt")
            let urlToFile = URL(fileURLWithPath: fileMan!, isDirectory: true)
            
            
            let textOfJSON: Any = try String(contentsOf: urlToFile, encoding: .utf8).data(using: .utf8)
            
            let codableInstance = try CodableMaker.init(to: textOfJSON)
            
        }catch let err{
            print(err)
        }
    }
    
    


}

