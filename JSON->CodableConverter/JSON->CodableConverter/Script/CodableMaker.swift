//
//  CodableMaker.swift
//  JSON->CodableConverter
//
//  Created by Piotr Krzesaj on 14/01/2018.
//  Copyright © 2018 pkTeam. All rights reserved.
//

import Foundation

extension String{
    
}

public enum CodableMakerErrors: Error{
    case invalidInputFormat
    case invalidSingleKeyFormat(Error)
}



class CodableMaker{
    
    /// Generating instance to generate files from JSON with codable implementation.
    ///
    /// - Parameter codable: JSON to interpretate
    /// - Throws: CodableMakerErrors
    init(to codable: Any) throws {
        guard let dict = try JSONSerialization.jsonObject(with: codable as! Data, options: .allowFragments) as? [String:Any] else {
            throw CodableMakerErrors.invalidInputFormat
        }
        var preparedJSONToConvert: [JSONWithType] = []
        //MARK: First stage. -> Converting JSON to our struct with simple types
        do{
            guard let d: [JSONWithType] = try generateJSONWithTypes(to: dict) else {
                throw CodableMakerErrors.invalidInputFormat
            }
            var jsonWithJsonsInside = d
            
            while !checkCodableCompatibility(check: jsonWithJsonsInside){
                do{
                    jsonWithJsonsInside = try convertClosestNonConvertedJSON(data: jsonWithJsonsInside)
                }catch let error{
                    throw error
                }
            }
            
            preparedJSONToConvert = jsonWithJsonsInside
            
        }catch let error{
            throw error
        }
        //MARK: Second stage. -> Creating files with structures compatibilet with Codable protocol
        
    }
    
    func generateJSONWithTypes(to data: [String:Any]) throws -> [JSONWithType]? {
        var JSONWithTypeReturnVar: [JSONWithType] = []
        try data.forEach{ key, value in
            let typeOfValue: CheckedType = checkType(for: value)
            if typeOfValue != .none{
            JSONWithTypeReturnVar.append(JSONWithType(typeOfValue: typeOfValue, key: key, value: value))
            }else{
                throw NSError(domain: key, code: 1, userInfo: [key: value])
            }
        }
        
        return JSONWithTypeReturnVar
    }
    

    
    struct JSONWithType{
        var typeOfValue: CheckedType
        var key: String
        var value: Any
        
        init(typeOfValue type: CheckedType,key k: String,value v: Any) {
            typeOfValue = type
            key = k
            value = v
        }
    }
    
}
