//
//  TypeChecker.swift
//  JSON->CodableConverter
//
//  Created by Piotr Krzesaj on 14/01/2018.
//  Copyright © 2018 pkTeam. All rights reserved.
//

import Foundation


extension CodableMaker{
    enum CheckedType{
        case string, int, float, double, none, anotherJSON, stringArray, intArray, doubleArray, floatArray
    }
    
    /// Checking type of value
    ///
    /// - Parameter check: value to check
    /// - Returns: CheckedType
    func checkType(for check: Any) -> CheckedType{
        if check as? Int != nil{
            return .int
        }else if check as? Float != nil{
            return .float
        }else if check as? Double != nil{
            return .double
        }else if check as? String != nil{
            return .string
        }else if check as? [String] != nil{
            return .stringArray
        }else if check as? [Int] != nil{
            return .intArray
        }else if check as? [Double] != nil{
            return .doubleArray
        }else if check as? [Float] != nil{
            return .floatArray
        }else if check as? [String: Any] != nil{
            return .anotherJSON
        }else{
            return .none
        }
    }
}
