//
//  JSONConverterToCodableCompatibility.swift
//  JSON->CodableConverter
//
//  Created by Piotr Krzesaj on 14/01/2018.
//  Copyright © 2018 pkTeam. All rights reserved.
//

import Foundation


extension CodableMaker{
    
    func convertClosestNonConvertedJSON(data toConvert: [JSONWithType]) throws -> [JSONWithType]{
        var jsonToReturn = toConvert
        try toConvert.forEach{
            if $0.typeOfValue == .anotherJSON{
                if let data = $0.value as? [JSONWithType]{
                    let keyMother = $0.key
                    if let i = jsonToReturn.index(where: { keyMother == $0.key }){
                        jsonToReturn[i].value = try whileNestings(data: data)
                    }
                }else{
                    if let data = $0.value as? [String:Any] {
                        if let newData = try generateJSONWithTypes(to: data){
                            let keyOfMother = $0.key
                            if let i = jsonToReturn.index(where: { ($0.key == keyOfMother) }){
                                jsonToReturn[i].value = newData
                            }
                        }
                    }
                }
            }
        }
        
        
        return jsonToReturn
    }
    
    private func whileNestings(data toConvert: [JSONWithType]) throws -> [JSONWithType]{
        var jsonToReturn = toConvert
        try toConvert.forEach{
            if $0.typeOfValue == .anotherJSON{
                if let data = $0.value as? [JSONWithType]{
                    let keyMother = $0.key
                    if let i = jsonToReturn.index(where: { ($0.key == keyMother) }){
                        jsonToReturn[i].value = try whileNestings(data: data)
                    }
                }else{
                    if let data = $0.value as? [String:Any]{
                        if let newData = try generateJSONWithTypes(to: data){
                            let keyMother = $0.key
                            if let i = jsonToReturn.index(where: { ($0.key == keyMother) }){
                                jsonToReturn[i].value = newData
                            }
                        }
                    }
                }
            }
        }
        
        
        return jsonToReturn
    }
    
    
}
