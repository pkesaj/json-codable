//
//  CodableCompatibilityChecker.swift
//  JSON->CodableConverter
//
//  Created by Piotr Krzesaj on 14/01/2018.
//  Copyright © 2018 pkTeam. All rights reserved.
//

import Foundation


extension CodableMaker{
    
    /// Checking codable protocol compatibility
    ///
    /// - Parameter check: chack json ;)
    /// - Returns: true -> Codable ready, false -> You need to check convert it again :/
    func checkCodableCompatibility(check: [JSONWithType]) -> Bool{
        var valueToReturn: Bool = false
        check.forEach{
            if $0.typeOfValue == .anotherJSON{
                if let jsonToCheck = $0.value as? [JSONWithType]{
                    if checkJSONNestings(value: jsonToCheck){
                        valueToReturn = true
                        return
                    }
                }
            }
        }
        
        return valueToReturn
    }
    
    /// Checking json but in nestings.
    ///
    /// - Parameter value: data to check ;)
    /// - Returns: true means Codable ready!
    private func checkJSONNestings(value: Any) -> Bool{
        var toReturn: Bool = true
        if let withTypes = value as? [JSONWithType] {
            withTypes.forEach{
                if $0.typeOfValue == .anotherJSON{
                    if !checkJSONNestings(value: $0.value){
                        toReturn = false
                        return
                    }
                }
            }
        }else{
            toReturn = false
        }
        return toReturn
    }
    
    
}
